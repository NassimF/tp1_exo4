#!/usr/bin/env python3
 
from setuptools import setup, find_packages
 
setup(
    name='MyCalculator',
    version='1.0.0',
    plateformes = 'LINUX',
    packages=find_packages(),
    packages_dir = {'' : 'MyCalculator'},
    author='NassimF',
    description='App Calculatrice',
    url='https://labomedia.org', 
    download_url='https://github.com/sergeLabo/pymultilame',
    license='NO LICENSE',
    keywords = ["soustraction", "somme", "division","multiplication"],
    classifiers = [ "Programming Language :: Python :: 3",
                    "Development Status :: 4 - Beta",
                    "Intended Audience :: Developers",
                    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
                    "Operating System :: Debian",
                    "Topic :: Blender Game Engine",
                    "Topic :: Network",
                    "Topic :: System"],
    long_description=open('README.md').read()
    )


























